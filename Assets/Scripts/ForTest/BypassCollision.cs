﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BypassCollision : MonoBehaviour
{
    public Collider ground;

    public void EnableCollideFor(Collider other)
    {
        Physics.IgnoreCollision(ground, other, false);
    }

    public void DisableCollideFor(Collider other)
    {
        Physics.IgnoreCollision(ground, other, true);
    }

    private void Update()
    {
        if (!Input.GetMouseButtonDown(0)) return;
        if (Camera.main == null) return;
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        RaycastHit hit;
        if (!Physics.Raycast(ray, out hit)) return;
        Collider hitTarget = hit.collider;
        this.DisableCollideFor(hitTarget);
    }
}