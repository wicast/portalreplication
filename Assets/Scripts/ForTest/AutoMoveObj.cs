﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoMoveObj : MonoBehaviour
{

	public Vector3 vec;
	[SerializeField,Range(0,1)] private float strength = 1;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		gameObject.transform.position += vec * Time.deltaTime * strength;
	}
}
