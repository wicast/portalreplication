﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InverseTransformVectorTest : MonoBehaviour
{

	public Transform player;
	public Transform lookAtRef;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		Vector3 tr=gameObject.transform.InverseTransformVector(player.forward);
		lookAtRef.localRotation = Quaternion.LookRotation(tr);
		Debug.Log(tr);
	}
}
