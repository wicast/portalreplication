﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraStencilPortalRenderTest : MonoBehaviour
{

	public Material originMat;
	public Material replaceMat;
	public GameObject portalGateWatched;

	private Renderer _renderer;
	
	// Use this for initialization
	void Start ()
	{
		_renderer = portalGateWatched.GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void OnPreRender()
	{
		_renderer.material = replaceMat;
	}

	private void OnPostRender()
	{
		_renderer.material = originMat;
	}
}
