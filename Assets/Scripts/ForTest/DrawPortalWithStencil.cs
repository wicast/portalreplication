﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DrawPortalWithStencil : MonoBehaviour
{

	public Material renderMat;

	public RenderTexture PreTexture
	{
		get { return preTexture; }
		set { preTexture = value; }
	}

	public RenderTexture preTexture;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void OnPostRender()
	{
		Graphics.SetRenderTarget(preTexture.colorBuffer, preTexture.depthBuffer);
		Graphics.Blit(Camera.current.activeTexture,preTexture, renderMat);
	}
}
