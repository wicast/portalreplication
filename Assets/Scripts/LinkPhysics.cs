﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public class LinkPhysics : MonoBehaviour
{
    private Rigidbody myRig;
    private GameObject brother;
    private LinkPhysics broLinkPhysics;

    // Use this for initialization
    void Start()
    {
//		brotherRigbody = brother.GetComponent<Rigidbody>();
        myRig = gameObject.GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
    }

//	private void OnCollisionEnter(Collision other)
//	{
////		Debug.Log("Me" + gameObject.name);
//		if (!brotherRigbody) return;
//		brotherRigbody.velocity = myRig.velocity;
//		brotherRigbody.rotation = myRig.rotation;
//
//	}

    private void OnCollisionStay(Collision other)
    {
//		Debug.Log("other" + other.gameObject.name);
        if (broLinkPhysics != null
            &&( Math.Abs(myRig.velocity.x) > 0.01
            || Math.Abs(myRig.velocity.y) > 0.01
            || Math.Abs(myRig.velocity.z) > 0.01))
        {
            broLinkPhysics.SyncPhy(myRig.velocity, myRig.rotation);
        }
    }

    public void SetBrother(GameObject bro)
    {
        brother = bro;
        broLinkPhysics = bro.GetComponent<LinkPhysics>();
    }

    public Object GenBrother(Vector3 position)
    {
        if (brother != null)
        {
            return brother;
        }

        brother = Instantiate(gameObject, position, Quaternion.identity);

        broLinkPhysics = brother.GetComponent<LinkPhysics>();
        broLinkPhysics.SetBrother(gameObject);
        return brother;
    }

    public void SyncPhy(Vector3 velocity, Quaternion rotation)
    {
        myRig.velocity = velocity;
        myRig.rotation = rotation;
    }
}