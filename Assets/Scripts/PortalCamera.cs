using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalCamera : MonoBehaviour
{
    public Transform playerCamera;
    public Transform portal;
    public Transform otherPortal;

    // Update is called once per frame
    void Update()
    {
        Vector3 playerOffsetFromPortal = otherPortal.InverseTransformPoint(playerCamera.position);
        transform.localPosition = Quaternion.Euler(0, 180, 0) * playerOffsetFromPortal;


        Vector3 toWord = Quaternion.Euler(0, 180, 0) * otherPortal.InverseTransformVector(playerCamera.forward);
        transform.localRotation = Quaternion.LookRotation(toWord, Vector3.up);
    }
}