﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PortalCamSetup : MonoBehaviour
{

	public Camera portalCam1;
	public Camera portalCam2;

	public Camera portalCam2B;

	public Material portalMat1;
	public Material portalMat2;

	// Use this for initialization
	void Start () {
		RenderTextureFormat format = RenderTextureFormat.Default;
		if(SystemInfo.SupportsRenderTextureFormat(RenderTextureFormat.ARGBHalf)){
			format = RenderTextureFormat.ARGBHalf;
		}
		
		
		if (portalCam1.targetTexture !=null) 
		{
			portalCam1.targetTexture.Release();
		}
		RenderTexture cam1Tex = new RenderTexture(Screen.width,Screen.height, 24,format);
		cam1Tex.Create();
		portalCam1.targetTexture = cam1Tex; 
		portalMat1.mainTexture = portalCam1.targetTexture;
		
		if (portalCam2.targetTexture !=null) 
		{
			portalCam2.targetTexture.Release();
		}
		RenderTexture cam2Tex = new RenderTexture(Screen.width,Screen.height, 24,format);
		cam2Tex.Create();
		portalCam2.targetTexture = cam2Tex;
		portalMat2.mainTexture = portalCam2.targetTexture;
		
		if (portalCam2B.targetTexture !=null) 
		{
			portalCam2B.targetTexture.Release();
		}
		RenderTexture cam2BTex = new RenderTexture(Screen.width,Screen.height, 24,format);
		portalCam2B.targetTexture = cam2BTex;
		portalCam2B.GetComponent<DrawPortalWithStencil>().preTexture = cam1Tex;

	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
